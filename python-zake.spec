%global _empty_manifest_terminate_build 0
Name:           python-zake
Version:        0.2.2
Release:        1
Summary:        A python package that works to provide a nice set of testing utilities for the kazoo library.
License:        ASL 2.0
URL:            https://github.com/yahoo/Zake
Source0:        https://files.pythonhosted.org/packages/ac/00/a322966639ce1b754475d2e83f169d32973be3ebbd6fc19344267d363627/zake-0.2.2.tar.gz
BuildArch:      noarch
%description
A python package that works to provide a nice set of testing utilities for the kazoo library.

%package -n python3-zake
Summary:        A python package that works to provide a nice set of testing utilities for the kazoo library.
Provides:       python-zake
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-kazoo
BuildRequires:  python3-six
# Requires for test
BuildRequires:  python3-testtools
BuildRequires:  python3-pytest
# General requires
Requires:       python3-kazoo
Requires:       python3-six
%description -n python3-zake
A python package that works to provide a nice set of testing utilities for the kazoo library.

%package help
Summary:        A python package that works to provide a nice set of testing utilities for the kazoo library.
Provides:       python3-zake-doc
%description help
A python package that works to provide a nice set of testing utilities for the kazoo library.

%prep
%autosetup -n zake-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
# the following test case is incompatible with newer kazoo
pytest -k 'not test_child_watch_no_create'

%files -n python3-zake -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Aug 09 2021 OpenStack_SIG <openstack@openeuler.org> - 0.2.2-1
- Package Spec generate
